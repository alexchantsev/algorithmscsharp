﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Algorithms.Sort
{
    public static partial class EnumerableExtensions
    {
        public static IReadOnlyCollection<T> BubbleSort<T>(this IEnumerable<T> source) where T : IComparable
        {
            var array = source.ToArray();
            int lastElementPosition = array.Length - 1;
            for (int i = 0; i < lastElementPosition; i++)
            {
                for (int j = 0; j < lastElementPosition - i; j++)
                {
                    if (array[j].CompareTo(array[j + 1]) > 0)
                    {
                        T temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
            return array;
        }
    }
}