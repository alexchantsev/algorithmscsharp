﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Sort
{
    public partial class EnumerableExtensions
    {
        public static IReadOnlyCollection<T> QuickSort<T>(this IEnumerable<T> source) where T : IComparable
        {
            T[] array = source.ToArray();
            InnerQuickSort(array, 0, array.Length - 1);
            return array;
        }

        private static int Partition<T>(T[] array, int start, int end) where T : IComparable
        {
            int marker = start;
            for (int i = start; i <= end; i++)
            {
                if (array[i].CompareTo(array[end]) <= 0)
                {
                    T temp = array[i];
                    array[i] = array[marker];
                    array[marker] = temp;
                    marker++;
                }
            }
            return marker - 1;
        }
        private static void InnerQuickSort<T>(T[] array, int start, int end) where T : IComparable
        {
            if (start >= end)
            {
                return;
            }
            int pivot = Partition(array, start, end);
            InnerQuickSort(array, start, pivot - 1);
            InnerQuickSort(array, pivot + 1, end);
        }
    }
}
