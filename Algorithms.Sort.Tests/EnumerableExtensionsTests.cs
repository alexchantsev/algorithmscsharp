﻿using NUnit.Framework;

namespace Algorithms.Sort.Tests
{
    [TestFixture]
    public class EnumerableExtensionsTests
    {
        int[] source = { 5, 4, 3, 2, 1 };
        int[] expected = { 1, 2, 3, 4, 5 };
        int[] emptySource = { };
        [Test]
        public void BubbleSort_Int32_Test()
        {
            var result = source.BubbleSort();
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void BubbleSort_Int32EmptyEnumerable_Test()
        {
            var result = emptySource.BubbleSort();
            Assert.Zero(result.Count);
        }

        [Test]
        public void QuickSort_Int32_Test()
        {
            var result = source.QuickSort();
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void QuickSort_Int32EmptyEnumerable_Test()
        {
            var result = emptySource.QuickSort();
            Assert.Zero(result.Count);
        }
    }
}
